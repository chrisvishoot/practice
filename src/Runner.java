
import Graph.MyGraph;
import Graph.MyWeightedGraph;
import Heap.Heap;
import LeetCode.*;
import MinBinaryHeap.MinBinaryHeap;
import MyHashMap.MyHashMap;
//import MyLinkedList.MyLinkedList;
import PrimAndDijkstra.Graph;
import Queue.MyQueue;
import Sorting.MergeSort;
import Tree.Tree;
import Trie.Trie;
import Tree.BST;
import MyLinkedList.LinkedListWithTail;

import java.util.*;

public class Runner {

    public static void main(String[] args) {
        MyCollection<?> collection = new MyCollection<>();
        Set set = collection.getSet();
    }
    public static void exists() {
        WordExists wordExists = new WordExists();
        wordExists.exist(null, "ABCCED");

    }
    public static void triple() {

    }
    public static void replacement() {
        int res = CharacterReplacement.findLength("abccde", 1);
        System.out.println(res);
    }
    public static void suggestion() {
//        WordTrie wordTrie = new WordTrie();
//        wordTrie.add("chris");
//        wordTrie.add("christopher");
//        wordTrie.add("christ");
//        wordTrie.add("a");
//        wordTrie.add("ab");
//        wordTrie.add("abc");
//        boolean isInTree = wordTrie.prefix("az");
//        System.out.println(isInTree);
//        System.out.println(wordTrie.getWords());
        String[] products = {"mobile","mouse","moneypot","monitor","mousepad"};
        Suggestion suggestion = new Suggestion();
        suggestion.suggestedProducts(products, "mouse");
    }


    public static void maxProd() {
        MaxProduct maxProduct = new MaxProduct();
        int[] nums = new int[] {2,-5,-2,-4,3};
        maxProduct.maxProduct(nums);
    }

//    public static void ebay() {
//        JavaTestEbay ebay = new JavaTestEbay();
//        String res = ebay.reformatString("chris123");
////        System.out.println(res);
//
//    }

    public static void topKWords() {
        TopKWords topKWords = new TopKWords();
        String[] input = {"anacell", "cetracular", "betacellular"};
        System.out.println(topKWords.getWords(input, 2));
    }

    //Look back into this, it's still a valid question! And i almost got it, good job Chris
    public static void longestSubString() {
        //"pwwkew"
        LongestSubString longestSubString = new LongestSubString();
        int longest = longestSubString.lengthOfLongestSubstring("vishoot");
        System.out.println(longest);
    }

    public static void ramp() {
        MaxWidth maxWidth = new MaxWidth();
        int[] A = {6,0,8,2,1,5};
        maxWidth.maxWidthRamp(A);
    }

    public static void equalDigit() {
        EqualDigitSum equalDigitSum = new EqualDigitSum();
        int[] input = {51, 32, 43};
        int result = equalDigitSum.equalDigit(input);
        System.out.println(result);
    }

    public static void bitFun() {
        BitFun bitFun = new BitFun();
        int res = bitFun.divide(-10, 3);
        System.out.println(res);
    }

    public static void prime() {
        PrimeNumber primeNumber = new PrimeNumber();

        System.out.println(primeNumber.primeCount(100));
    }
    public static void majorityElement() {
        MajorityElement majorityElement = new MajorityElement();
        int[] in = {3,2,3};
        System.out.println(majorityElement.majorityElement(in));
    }

    public static void tempCheck() {
//        int[] t = {73, 74, 75, 71, 69, 72, 76, 73};
//        permute();
//        threeSum();
    }

    public static void rain() {
        int[] height = {0,1,0,2,1,0,1,3,2,1,2,1};
        RainWater rainWater = new RainWater();
        rainWater.trap(height);
    }

    public static void threeSum() {
        ThreeSum threeSum = new ThreeSum();
//        int[] nums = {-1, 0, 1, 2, -1, -4};
        int[] nums = {1, 0, -1, 0, -2, 2};
        threeSum.fourSum(nums, 0);

//        int index = 3;
//        int[] indexes = threeSum.twoSum(nums, nums[index] * -1, index);
//        System.out.println(Arrays.toString(indexes));
//        int res = nums[indexes[0]] + nums[indexes[1]] + nums[index];
//        System.out.println(res);
    }

    public static void permute() {
        StringPermutation stringPermutation = new StringPermutation();
        String test = "CHRIS";
//        test = stringPermutation.swap(0, 1, test);
//        System.out.println(test);
//        stringPermutation.replaceAtIndex(test, 'a', 0);
        stringPermutation.printPermutation(test);
    }

    public static void islandCount() {
        IslandCount islandCount = new IslandCount();
        //11110
        //[["1","0","1","1","1"],["1","0","1","0","1"],["1","1","1","0","1"]]
        char[][] grid = {{'1','0','1','1', '1'},
                {'1','0','1','0', '1'},
                {'1','1','1','0', '1'}};
        int count = islandCount.numIslands(grid);
        System.out.println(count);
    }
//    public static void intersection() {
//        Intersection intersection = new Intersection();
//        int[] first = {1,3};
//        int[] second = {4,6};
//        System.out.println(intersection.isIntersect(first,second));
//    }

    public static void atoi() {
        Atoi atoi = new Atoi();
        System.out.println(atoi.myAtoi("4193 with words"));
    }


    public static void integerToWords() {
        IntegerToEnglishWords integerToEnglishWords = new IntegerToEnglishWords();
        integerToEnglishWords.numberToWords(123435);
    }

    public static void pqFun() {
        TopKWords topKWords = new TopKWords();
        String[] words = {"i", "love", "leetcode", "i", "love", "coding"};
        topKWords.getWords(words, 2);
    }

    public static void binarySearchTree() {
        BST tree = new BST();
        tree.insert(4);
        tree.insert(3);
        tree.insert(2);
        tree.insert(1);
        tree.inOrder();
        tree.bfs();
    }

    public static void valid() {
        ValidParentheses validParentheses = new ValidParentheses();
        String input = "[()]";
        boolean valid = validParentheses.isValid(input);
        System.out.println(valid);
    }


    public static void mst() {
        int vertices = 6;
        Graph graph = new Graph(vertices);
        graph.addEdge(0, 1, 4);
        graph.addEdge(0, 2, 3);
        graph.addEdge(1, 2, 1);
        graph.addEdge(1, 3, 2);
        graph.addEdge(2, 3, 4);
        graph.addEdge(3, 4, 2);
        graph.addEdge(4, 5, 6);
        graph.primMST();
    }


    public static void minBinaryHeap() {
        MinBinaryHeap<String> test = new MinBinaryHeap<String>();
        test.add(5, "chris");
        test.add(6, "vishoot");
        test.add(1, "small value");
        test.add(0, "smallest value");
        test.add(Integer.MAX_VALUE, "biggest value");

        test.decrease("biggest value", Integer.MIN_VALUE);
        test.extractMinNode();
        System.out.println(test.toString());

    }

    public static void trie() {
         Trie trie = new Trie();
         trie.insert("abc");
         trie.insert("chris");
         trie.insert("vishoot");
        trie.insert("penguin");
        trie.insert("memes");
         List<String> words = trie.getWordList();
         System.out.println(words);
         System.out.println(trie.search("memes"));
         trie.delete("memes");
        System.out.println(trie.search("memes"));
    }
    public static void heap() {
         Heap heap = new Heap();
         heap.add(100);
        heap.add(25);
        heap.add(900);
        int result = heap.poll();
        heap.poll();
        heap.printHeap();
    }

    public static void weightGraph() {
        int vertices = 6;
        MyWeightedGraph graph = new MyWeightedGraph(vertices);
        graph.addEdge(0, 1, 4);
        graph.addEdge(0, 2, 3);
        graph.addEdge(1, 3, 2);
        graph.addEdge(1, 2, 5);
        graph.addEdge(2, 3, 7);
        graph.addEdge(3, 4, 2);
        graph.addEdge(4, 0, 4);
        graph.addEdge(4, 1, 4);
        graph.addEdge(4, 5, 6);
        graph.printList();

        graph.topSort(0);
    }

    public static void graph() {
        MyGraph<String> graph = new MyGraph<>();
        graph.addVertex("Chris");
        graph.addVertex("Vishoot");
        graph.addVertex("Memes");
        graph.addEdge("Chris", "blah", false);
        graph.addEdge("Chris", "blah1", false);
        graph.addEdge("Chris", "blah2", true);
        graph.printMap();
    }

    public static void hashMap() {
        MyHashMap<String, String> test = new MyHashMap<String, String>();
        test.add("one", "World");
        test.add("two", "World");
        test.add("three", "World");
        test.add("four", "World");
        test.add("five", "World");
        test.add("six", "World");
        test.add("seven", "World");
        test.add("eight", "World");
        test.add("nine", "World");
        test.add("ten", "Ten");

        System.out.println("Current Size "  + test.getSize());
        System.out.println("Current Capacity " + test.getCapacity());
        System.out.println(test.toString());
        String result = test.get("ten");
        System.out.println(result);

    }

    public static void binaryTree() {
        Tree tree = new Tree();
        tree.add(3);
        tree.add(2);
        tree.add(4);
        tree.inOrder();
        tree.postOrder();
        tree.preOrder();

    }

    public static void mergeSort(int[] A) {
        MergeSort mergeSort = new MergeSort();
        mergeSort.mergeSort(A, A.length);

    }


    public static void queueTest() {
        MyQueue<String> q = new MyQueue<>("hello");
        q.add("chris");
        q.add("vishoot");
        q.add("was here");
        q.add(null);
        String data = q.dequeue();
        System.out.println(data);


    }

//    public static void linkedListTests() {
//        MyLinkedList list = new MyLinkedList("Chris");
//        list.add("Vishoot");
//        list.add("says hi");
//        list.add("Dank memes");
//        list.remove("says hi");
////        list.printList();
//
//        list.reverse(list.root);
//    }


}
