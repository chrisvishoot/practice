package PrimAndDijkstra;

import javafx.util.Pair;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class Graph {
    int vertices;
    LinkedList<Edge>[] adjacencylist;

    public Graph(int vertices) {
        this.vertices = vertices;
        this.adjacencylist = new LinkedList[this.vertices];
        for(int i = 0; i < adjacencylist.length; i++) {
            adjacencylist[i] = new LinkedList<>();
        }
    }
    public void addEdge(int source, int destination, int weight) {
        LinkedList<Edge> linkedList = this.adjacencylist[source];
        Edge edge = new Edge(source, destination, weight);
        linkedList.add(edge);
        this.adjacencylist[source] = linkedList;

        edge = new Edge(destination, source, weight);
        linkedList = this.adjacencylist[destination];
        linkedList.add(edge);
        this.adjacencylist[destination] = linkedList;

    }

    public void primMST() {
        boolean[] mst = new boolean[this.vertices];
        ResultSet[] resultSets = new ResultSet[this.vertices];
        int[] keys = new int[this.vertices];

        for(int i = 0; i < this.vertices; i++) {
            resultSets[i] = new ResultSet();
            keys[i] = Integer.MAX_VALUE;
        }

        PriorityQueue<Pair<Integer, Integer>> pq = new PriorityQueue<>(vertices, new Comparator<Pair<Integer, Integer>>() {
            @Override
            public int compare(Pair<Integer, Integer> o1, Pair<Integer, Integer> o2) {
                int key1 = o1.getKey();
                int key2 = o2.getKey();
                return key1 - key2;
            }
        });
        keys[0] = 0;
        Pair<Integer, Integer> p0 = new Pair(0,0);
        pq.offer(p0);
        resultSets[0] = new ResultSet();
        resultSets[0].parent = -1;

        while(!pq.isEmpty()) {
            Pair<Integer, Integer> extractedPair = pq.poll();
            int extractedVertex = extractedPair.getValue();
            mst[extractedVertex] = true;
            LinkedList<Edge> list = this.adjacencylist[extractedVertex];
            for(Edge edge : list) {
                //Only do operation on destinations that aren't apart of the MST
                if(mst[edge.destination] == false) {
                    int destination = edge.destination;
                    int newKey = edge.weight;
                    //If the current solution has a greater weight
                    if(keys[destination] > newKey) {
                        Pair<Integer, Integer> p = new Pair<Integer, Integer>(newKey, destination);
                        pq.offer(p);
                        resultSets[destination].parent = destination;
                        resultSets[destination].weight = newKey;
                    }

                }
            }
        }
    printMST(resultSets);
    }


    public void dijkstrasAlgorithm(int vertex) {
        boolean[] inSPT = new boolean[this.vertices];
        int[] distance = new int[this.vertices];

        for(int i = 0; i < this.vertices; i++) {
            distance[i] = Integer.MAX_VALUE;
        }

        PriorityQueue<Pair<Integer, Integer>> pq = new PriorityQueue<>(vertices, new Comparator<Pair<Integer, Integer>>() {
            @Override
            public int compare(Pair<Integer, Integer> o1, Pair<Integer, Integer> o2) {
                int key1 = o1.getKey();
                int key2 = o2.getKey();
                return key1 - key2;
            }
        });
        distance[0] = 0;
        Pair<Integer, Integer> p0 = new Pair<Integer, Integer>(distance[0], 0);
        pq.offer(p0);
        while(!pq.isEmpty()) {
            Pair<Integer, Integer> extractedPair = pq.poll();
            int extractedVertex = extractedPair.getValue();
            LinkedList<Edge> linkedList = this.adjacencylist[extractedVertex];
            for(Edge edge : linkedList) {
                int destination = edge.destination;
                if(!inSPT[destination]) {
                    int newKey = distance[destination] + edge.weight;
                    int currentKey = distance[destination];
                    if(currentKey > newKey) {
                        Pair<Integer, Integer> p = new Pair<Integer, Integer>(newKey, destination);
                        pq.offer(p);
                        distance[destination] = newKey;
                    }
                }
            }
        }

    }


    public void printMST(ResultSet[] resultSet) {
        int total_min_weight = 0;
        System.out.println("Minimum Spanning Tree: ");
        for (int i = 1; i < vertices; i++) {
            System.out.println("Edge: " + i + " - " + resultSet[i].parent +
                    " key: " + resultSet[i].weight);
            total_min_weight += resultSet[i].weight;
        }
        System.out.println("Total minimum key: " + total_min_weight);
    }
}
