package Queue;

public class Node<Data> {
    Data data;
    Node<Data> next;
    public Node(Data data) {
        this.data = data;
    }
}
