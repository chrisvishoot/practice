package Queue;

public class MyQueue<Data> {
    Node<Data> front;

    public MyQueue(Data data) {
        Node<Data> node = new Node(data);
        this.front = node;
    }
    public void add(Data input) {
        if(input == null) {
            return;
        }
        Node<Data> newNode = new Node(input);
        Node<Data> current = this.front;
        while(current.next != null) {
            current = current.next;
        }
        current.next = newNode;
    }

    public void printQueue() {
        Node<Data> current = this.front;
        while(current != null) {
            System.out.println(current.data);
            current = current.next;
        }
    }

    public Data dequeue() {
        Node<Data> currentFront = this.front;
        this.front = this.front.next;
        return currentFront.data;
    }




}
