package Heap;

import java.util.Arrays;

public class Heap {
    private int capacity = 10;
    private int size = 0;
    private int[] items = new int[capacity];




    public void printHeap() {
        System.out.println(Arrays.toString(items));
    }

    public int poll() {
        if(size == 0) throw new IllegalStateException();
        int item = items[items.length - 1];
        size--;
        items[0] = items[items.length - 1];
        heapifyDown();
        return item;
    }



    public void add(int element) {
        ensureCapacity();
        items[size++] = element;
        heapifyUp();
    }

    public int peek() {
        if(size == 0) throw new IllegalStateException();
        return items[0];
    }


    private void heapifyUp() {
        int index = size-1;
        while(hasParent(index) && parent(index) < items[index]) {
            swap(getParentIndex(index), index);
            index = getParentIndex(index);
        }
    }

    private void heapifyDown() {
        int index = 0;
        while(hasLeftChild(index)) {
            int smallerChildIndex = getLeftChildIndex(index);
            if(hasRightChild(index) && rightChild(index) < leftChild(index)) {
                smallerChildIndex = getRightChildIndex(index);
            }

            if(items[index] < items[smallerChildIndex]) {
                break;
            } else {
                swap(index, smallerChildIndex);
                index = smallerChildIndex;
            }
        }
    }


    private void swap(int indexOne, int indexTwo) {
        int temp = items[indexOne];
        items[indexOne] = items[indexTwo];
        items[indexTwo] = temp;
    }

    private void ensureCapacity() {
        if(size == capacity) {
            capacity *= 2;
            items = Arrays.copyOf(items, capacity);
        }
    }

    private int getLeftChildIndex(int parent) {
        return 2 * parent + 1;
    }
    private int getRightChildIndex(int parent) {
        return 2 * parent + 2;
    }
    private int getParentIndex(int child) {
        return (child-1) / 2;
    }

    private boolean hasLeftChild(int parent) {
        return getLeftChildIndex(parent) < size;
    }
    private boolean hasRightChild(int parent) {
        return getRightChildIndex(parent) < size;
    }
    private boolean hasParent(int child) {
        return getParentIndex(child) >= 0;
    }

    private int leftChild(int parent) {
        return items[getLeftChildIndex(parent)];
    }

    private int rightChild(int parent) {
        return items[getRightChildIndex(parent)];
    }
    private int parent(int child) {
        return items[getParentIndex(child)];
    }






}
