package LRU;

public class Node {
    Node previous;
    Node next;
    int key;
    int value;
    public Node() {}
    public Node(int key, int value) {
        this.key = key;
        this.value = value;
    }
}
