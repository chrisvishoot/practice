package LRU;

import java.util.HashMap;
import java.util.Map;


public class LRUCache {
    public class Node {
        Node previous;
        Node next;
        int key;
        int value;
        public Node() {}
        public Node(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }
    Map<Integer, Node> map;
    Node head;
    Node tail;
    int capacity;
    int size;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>();
        this.size = 0;
    }

    public int get(int key) {
        if(!this.map.containsKey(key)) {
            return -1;
        }
        Node target = this.map.get(key);

        remove(target);
        addToTail(target);
        return target.value;

    }

    public void put(int key, int value) {
        if(this.map.containsKey(key)) {
            Node target = new Node(key, value);
            remove(target);
            addToTail(target);
        } else {
            if(map.size() > capacity) {
                Node head = map.get(this.head.key);
                remove(head);
            }
            Node target = new Node(key,value);
            addToTail(target);
            this.map.put(key, target);
        }


    }

    private void remove(Node n){
        if(n.previous!=null){
            n.previous.next = n.next;
        }else{
            head = n.next;
        }

        if(n.next!=null){
            n.next.previous = n.previous;
        }else{
            tail = n.previous;
        }
    }

    private void addToTail(Node n){
        if(tail!=null){
            tail.next = n;
        }

        n.previous = tail;
        n.next = null;
        tail = n;

        if(head == null){
            head = tail;
        }
    }
}


