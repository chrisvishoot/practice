package MinBinaryHeap;

public class Node<T> {
    T key;
    int weight;
    public Node(T key, int weight) {
        this.key = key;
        this.weight = weight;
    }
    public Node() {

    }

    @Override
    public String toString() {
        return "Node{" +
                "key=" + key +
                ", weight=" + weight +
                '}';
    }
}
