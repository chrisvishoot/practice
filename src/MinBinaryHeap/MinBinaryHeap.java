package MinBinaryHeap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Date 04/06/2013
 * @author Tushar Roy
 *
 * Data structure to support following operations
 * extracMin - O(logn)
 * addToHeap - O(logn)
 * containsKey - O(1)
 * decreaseKey - O(logn)
 * getKeyWeight - O(1)
 *
 * It is a combination of binary heap and hash map
 *
 */
public class MinBinaryHeap<T> {
    private List<Node> allNodes = new ArrayList<>();
    private Map<T, Integer> nodePosition = new HashMap<>();

    /**
     * Add key and its weight to they heap
     */
    public void add(int weight,T key) {
        Node<T> node = new Node();
        node.weight = weight;
        node.key = key;
        allNodes.add(node);
        int size = allNodes.size();
        int current = size - 1;
        int parentIndex = (current - 1) / 2;
        nodePosition.put(node.key, current);
        perculateData(parentIndex, current);

    }

    private void perculateData(int parentIndex, int current) {
        while (parentIndex >= 0) {
            Node<T> parentNode = allNodes.get(parentIndex);
            Node<T> currentNode = allNodes.get(current);
            if (parentNode.weight > currentNode.weight) {
                swap(parentNode,currentNode);
                updatePositionMap(parentNode.key,currentNode.key,parentIndex,current);
                current = parentIndex;
                parentIndex = getParentIndex(parentIndex);
            } else {
                break;
            }
        }
    }


    private void updatePositionMap(T firstData, T secondData, int firstPosition, int secondPosition){
        nodePosition.remove(firstData);
        nodePosition.remove(secondData);
        nodePosition.put(firstData, firstPosition);
        nodePosition.put(secondData, secondPosition);
    }




    public void decrease(T key, int newWeight) {
        Integer position = nodePosition.get(key);
        allNodes.get(position).weight = newWeight;
        int parentIndex = getParentIndex(position);
        perculateData(parentIndex, position);
    }

    public Integer getWeight(T key) {
        Integer position = nodePosition.get(key);
        if(position == null && position < 0) {
            return null;
        } else{
            return allNodes.get(position).weight;
        }
    }



    private void swap(Node<T> firstNode, Node<T> secondNode) {
        T tempKey = firstNode.key;
        int tempWeight = firstNode.weight;

        firstNode.key = secondNode.key;
        firstNode.weight = secondNode.weight;

        secondNode.key = tempKey;
        secondNode.weight = tempWeight;

    }
    private int getParentIndex(int child) {
        return (child - 1) / 2;
    }


    public T getMin() {
        if(allNodes.size() > 0) {
            return (T)allNodes.get(0).key;
        } else {
            throw new IllegalStateException("Empty Array");
        }
    }

    public boolean isEmpty() {
        return allNodes.size() == 0;
    }
    public void extractMinNode() {

        int size = allNodes.size() - 1;
        //First we get the min node value, which is the at index 0
        Node<T> minNode = allNodes.get(0);
        //Then we replace the allNodes.get(0) with the last node in the arraylist
        int lastNodeWeight = allNodes.get(size).weight;
        allNodes.get(0).weight = lastNodeWeight;
        allNodes.get(0).key = allNodes.get(size).key;
        nodePosition.remove(minNode.key);
        nodePosition.put((T)allNodes.get(0).key, 0);
        allNodes.remove(size);

        int currentIndex = 0;
        size--;
        while(true) {
            int left = getLeftChild(currentIndex);
            int right = getRightChild(currentIndex);
            if(left > size) {
                break;
            }
            if(left > right) {

            }
        }

    }


    private int getLeftChild(int parent) {
        return (2 * parent) + 1;
    }

    private int getRightChild(int parent) {
        return (2 * parent) + 2;
    }

    @Override
    public String toString() {
        return "MinBinaryHeap{" +
                "allNodes=" + allNodes +
                ", nodePosition=" + nodePosition +
                '}';
    }
}