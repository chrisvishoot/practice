package LeetCode;

import java.util.HashSet;
import java.util.Set;

public class MyCollection<T> {
    Set<T> set;

    public MyCollection() {
        this.set = new HashSet<>();
    }
    public Set<T> getSet() {
        return this.set;
    }
}
