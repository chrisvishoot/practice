package LeetCode;

import java.util.HashSet;
import java.util.Set;

public class StringPermutation {
    public void printPermutation(String input) {
        HashSet<String> words = new HashSet<>();
        permute(input, 0, input.length() -1);
        System.out.println(words.size());

    }
    private void permute(String str, int l, int r)
    {
        if (l == r)
            System.out.println(str);
        else
        {
            for (int i = l; i <= r; i++)
            {
                str = swap(i, l,str);
                permute(str, l+1, r);
            }
        }
    }

    public String swap(int first, int second, String word) {
        char temp = word.charAt(first);
        word = replaceAtIndex(word, word.charAt(second), first);
        word = replaceAtIndex(word, temp, second);
        return word;

    }
    public String replaceAtIndex(String word, char target, int index) {
        String res = word.substring(0, index) + target + word.substring(index + 1);
        return res;
    }
}
