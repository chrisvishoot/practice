package LeetCode;

import java.util.HashMap;

public class MajorityElement {
    public int majorityElement(int[] nums) {
        int threshold = (nums.length / 2);
        HashMap<Integer, Integer> count = new HashMap<Integer, Integer>();
        for(int n : nums) {
            if(count.containsKey(n)) {
                int c = count.get(n);
                c++;
                count.put(n, c);
                if(c > threshold) {
                    return n;
                }
            } else {
                count.put(n, 1);
            }
        }
        return -1;
    }
}
