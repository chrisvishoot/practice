package LeetCode;

import java.util.*;

public class ThreeSum {

    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> soln = new ArrayList<>();

        for(int i = 0; i < nums.length; i++) {
            int[] indexes = twoSum(nums, nums[i], i);
            if(indexes != null) {
                List<Integer> res = new ArrayList<>();

            }
        }
        System.out.println(soln);
        return soln;
    }

    public int[] twoSum(int[] nums, int target, int indexToSkip) {
        int[] soln = null;
        if(nums == null || nums.length == 0) {
            return soln;
        }
        HashMap<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++) {
            if(i == indexToSkip) continue;
            int difference = target - nums[i];
            if(map.containsKey(difference)) {
                soln = new int[2];
                soln[0] = i;
                soln[1] = map.get(difference);
                return soln;
            } else {
                map.putIfAbsent(nums[i], i);
            }
        }
        return soln;
    }
    public List<List<Integer>> fourSum(int[] nums, int target) {
        Arrays.sort(nums);
        List<List<Integer>> result = new ArrayList<>();
        HashSet<List<Integer>> hashset = new HashSet<>();
        //So we have a + b + c + d = target, going to re-arrange
        //so that we have a + b = target - (c + d)
        for(int i = 0; i < nums.length - 3; i++) {
            int c = nums[i];
            int d = nums[i+1];
            int difference = target - (c + d);
            int low = i+2;
            int high = nums.length - 1;
            while(low < high) {
                int sum = nums[low] + nums[high];
                if(sum > difference) {
                    high--;
                } else if(sum < difference) {
                    low++;
                } else {
                    hashset.add(Arrays.asList(
                            nums[low], nums[high], c, d
                    ));
                    low++;
                    high--;
                }
            }
        }
        for(List<Integer> list : hashset) {
            result.add(list);
        }
        return result;
    }
}
