package LeetCode;

import java.util.ArrayList;
import java.util.List;

public class PrimeNumber {
    public int primeCount(int n) {
        return getPrimeNumbers(n).size();

    }
    public List<Integer> getPrimeNumbers(int n) {
        List<Integer> list = new ArrayList<>();
        boolean prime[] = new boolean[n+1];
        for(int i=0;i<n;i++)
            prime[i] = true;

        for(int p = 2; p*p <=n; p++)
        {
            // If prime[p] is not changed, then it is a prime
            if(prime[p] == true)
            {
                // Update all multiples of p
                for(int i = p*p; i <= n; i += p)
                    prime[i] = false;
            }
        }

        // Print all prime numbers
        for(int i = 2; i <= n; i++)
        {
            if(prime[i] == true) {
                System.out.print(i + " ");
                list.add(i);
            }

        }

        return list;
    }
}
