package LeetCode;

import java.util.*;

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
};

class SumOfPathNumbers {
    public static int findSumOfPathNumbers(TreeNode root) {
        // TODO: Write your code here
        List<Integer> numbers = new ArrayList<>();
        int sum = 0;
        return findSumOfPathNumbers(root, numbers, sum);
    }
    public static int findSumOfPathNumbers(TreeNode current, List<Integer> numbers,
                                          int sum) {
        if(current == null) {
            return -1;
        }
        numbers.add(current.val);
        if(current.left == null && current.right == null) {
            System.out.println(numbers);
            int numberValue = getNumberValue(numbers);
            System.out.println(numberValue);
        }
        findSumOfPathNumbers(current.left, numbers, sum);
        findSumOfPathNumbers(current.right, numbers, sum);
        //Then we go left and right and do the calculation
        numbers.remove(numbers.size() - 1);
        return sum;
    }

    public static int getNumberValue(List<Integer> numbers) {
        int number = 0;
        int tensPlace = 0;
        for(int i = numbers.size()-1; i >= 0; i--) {
            int currentValue = numbers.get(i);
            System.out.println("current value " + currentValue);
            number += (numbers.get(i) + tensPlace * 10);
            System.out.println("Number so far " + number);
            tensPlace++;
        }
        return number;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(0);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(1);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(5);
        System.out.println("Total Sum of Path Numbers: " + SumOfPathNumbers.findSumOfPathNumbers(root));
    }
}

