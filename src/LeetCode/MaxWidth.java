package LeetCode;

public class MaxWidth {
    public int maxWidthRamp(int[] A) {
        int res = 0;
        for(int i = 0; i < A.length-1; i++) {
            int j = i + 1;
            if(A[j] >= A[i]) {
                int diff = j - i;
                res = Math.max(diff, res);
            }
        }
        return res;
    }

}
