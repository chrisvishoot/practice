//package LeetCode;
//
//import java.util.Arrays;
//import java.util.Comparator;
//import java.util.PriorityQueue;
//
//public class KClosestPoints {
//
//
//    public int[][] kClosest(int[][] points, int K) {
//        PriorityQueue<PointWrapper> pq = new PriorityQueue<>();
//        for(int i = 0; i < points.length; i++) {
//            double distance = calculateDistance(points[i]);
//            PointWrapper pointWrapper = new PointWrapper(points[i], distance);
//            pq.add(pointWrapper);
//        }
//        int[][] result = new int[K][K];
//        for(int i = 0; i < K; i++) {
//            int[] point = pq.poll().point;
//            result[i] = point;
//        }
//        return result;
//    }
//
//    public double calculateDistance(int[] point) {
//        return  Math.pow(point[0], 2) + Math.pow(point[1], 2);
//    }
//    class PointWrapper implements Comparator<PointWrapper> {
//        @Override
//        public int compare(PointWrapper o1, PointWrapper o2) {
//
//            return 0;
//        }
//    }
//    class PointWrapper implements Comparable<Integer>{
//        int[] point;
//        double distance;
//        public PointWrapper(int[] point, double distance) {
//            this.point = point;
//            this.distance = distance;
//        }
//
//        @Override
//        public int compareTo(Integer o) {
//            return 0;
//        }
//    }
//}
