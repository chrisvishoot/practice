package LeetCode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WordExists {
    public boolean exist(char[][] board, String word) {
        if(word == null || word.length() == 0) return false;
        if(board == null || board.length == 0 || board[0].length == 0) return false;
        //now to do the dfs
        Trie trie = new Trie();
        trie.add(word);
        System.out.println(trie.containsWord(word));
        return true;
    }
    public void helper(char[][] board, boolean[][] visited,
                       String word, int col, int row,
                      StringBuilder sb) {
        if(visited[col][row]) return;
        if(isSafe(board, row, col)) {
            //Go left

            //Go right
            //Go up
            //Go down
        } else {
            return;
        }

    }
    public boolean isSafe(char[][] board, int row, int col) {
        int rL = board.length;
        int cL = board[0].length;
        boolean rowSafe = row >= 0 && row < rL;
        boolean colSafe = col >= 0 && col < cL;
        return rowSafe && colSafe;
    }
    class TrieNode {
        HashMap<Character, TrieNode> children;
        boolean isWord;
        public TrieNode() {
            children = new HashMap<>();
        }
    }
    public class Trie {
        List<String> words;
        TrieNode root;
        public Trie() {
            this.root = new TrieNode();
            this.words = new ArrayList<>();
        }
        public void add(String in) {
            char[] array = in.toCharArray();
            TrieNode curr = this.root;
            for(char c : array) {
                if(!curr.children.containsKey(c)) {
                    curr.children.put(c, new TrieNode());
                }
                curr = curr.children.get(c);
            }
            curr.isWord = true;
            this.words.add(in);
        }


        public boolean containsWord(String word) {
            TrieNode curr = this.root;
            for(char c : word.toCharArray()) {
                if(!curr.children.containsKey(c)) {
                    return false;
                }
                curr = curr.children.get(c);
            }
            return curr.isWord;
        }
    }
}
