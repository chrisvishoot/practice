package LeetCode;

public class RainWater {

    public int trap(int[] height) {
        //We cannot start at 0, have to start at index 1
        //We have a look ahead, so we get the
        int first = 1;
        int next = getNextHeight(first, first+1, height);
        int volume = getVolume(first, next, height);
    return volume;
    }

    public int getNextHeight(int first, int next, int[] height) {
        while(!(height[next] >= height[first]) && next < height.length ) {
            next++;
        }
        if(height[next] < height[first]) {
            return 0;
        }
        return next;
    }

    public int getVolume(int first, int next, int[] height) {
        //Then we find the volume
        int deltaX = next - first;
        int deltaY = Math.min(height[next], height[first]);
        int volume = deltaX * deltaY;
        return volume;
    }
}
