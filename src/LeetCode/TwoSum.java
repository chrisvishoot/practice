package LeetCode;

import java.util.HashMap;

public class TwoSum {

    public int[] betterSoln(int[] nums, int target) {
        int[] soln = new int[2];
        if(nums == null) {
            return soln;
        }
        HashMap<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++) {
            int difference = target - nums[i];
            if(map.containsKey(difference)) {
                soln[0] = map.get(difference);
                soln[1] = i;
                break;
            } else {
                map.put(difference, i);
            }
        }
        return soln;
    }


    public int[] naiveSolution(int[] nums, int target) {
        int[] soln = new int[2];
        if(nums == null) {
            return soln;
        }
        for(int i = 0; i < nums.length; i++) {
            for(int j = i+1; j < nums.length; j++) {
                int sum = nums[i] + nums[j];
                if(sum == target) {
                    soln[0] = nums[i];
                    soln[1] = nums[j];
                    break;
                }
            }
        }
        return soln;
    }
}
