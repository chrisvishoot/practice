package LeetCode;

import java.util.*;


// Complete the following test using Java and return your working code.
// Put your code in only this file.
// Given a random alpha-numeric string with no special characters,
// reformat the string without adding or removing any characters so that no alphabet characters are
// adjacent to any other alphabet characters and no numeric characters are adjacent to any other numeric characters,
// if possible, and return the modified string. If it is not possible to reformat the string in that way,
// then group all the alphabet characters at the beginning of the string and all numeric characters at the end of
// the string. In either case keep all alphabet characters in the same order relative to each other before and after the
// formatting and also keep all numeric characters in the same order releative to each other before and after the formatting.

// Example input1: "aabb12cc345"
// Example output1: "a1a2b3b4c5c"
// Example input2: "a1aa"
// Example output2: "aaa1"
public class JavaTestEbay {
    private String reformatString(String str) {
        StringBuilder sb = new StringBuilder();
        //Edge case to see if the string input is null or empty
        if(str == null || str.length() == 0 || str.isEmpty()) {
            return sb.toString();
        }


        Queue<Character> charQueue = new LinkedList<>();
        Queue<Character> digitQueue = new LinkedList<>();
        for(char c : str.toCharArray()) {
            if(Character.isDigit(c)) {
                digitQueue.add(c);
            } else {
                charQueue.add(c);
            }
        }
        //So there are a few cases to take into account,
        //1) if they are the same size, or have a difference of one
        boolean sameSize = charQueue.size() == digitQueue.size();
        boolean diffOfOne = (charQueue.size() -1) == digitQueue.size() || charQueue.size() == (digitQueue.size() - 1);
        if(sameSize || diffOfOne) {
            boolean firstCharLetter = false;
            if(Character.isLetter(str.charAt(0))) {
                firstCharLetter = true;
            }
            //Then we can inter-weve between the two.
            //abcd1234 is an example input
            //a1b2c3d4 is the output if the two queues are the same
            while(!charQueue.isEmpty() && !digitQueue.isEmpty()) {
                char character = charQueue.remove();
                char digit = digitQueue.remove();
                if(firstCharLetter) {
                    sb.append(character);
                    sb.append(digit);
                } else {
                    sb.append(digit);
                    sb.append(character);
                }

            }
            while(!charQueue.isEmpty()) {
                sb.append(charQueue.remove());
            }
            while(!digitQueue.isEmpty()) {
                sb.append(digitQueue.remove());
            }
        } else {
            //so then we put all of the letters on the right, digits on the left
            while(!charQueue.isEmpty()) {
                sb.append(charQueue.remove());
            }
            while(!digitQueue.isEmpty()) {
                sb.append(digitQueue.remove());
            }
        }
        //if we don't have the same number, this is where i would ask some questions, going to email
        //the ebay folks about it.

        return sb.toString();
    }
}
