package LeetCode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TripletSum {
    public static List<List<Integer>> searchTriplets(int[] arr) {
        if(arr == null || arr.length < 3) return null;
        List<List<Integer>> triplets = new ArrayList<>();
        for(int i = 0; i < arr.length; i++) {
            //Then do the target algorithm, which is O(n) with a hashmap
            int target = arr[i] * -1;
            List<Integer> list = targetSum(i, target, arr);
            if(list != null) {
                list.add(target);
                if(!triplets.contains(list)) {
                    triplets.add(list);
                }
            }
        }
        return triplets;
    }
    public static List<Integer> targetSum(int indexToSkip, int target, int[] arr) {
        Map<Integer, Integer> map = new HashMap<>();
        List<Integer> soln = new ArrayList<Integer>();
        for(int i = 0; i < arr.length; i++) {
            if(i == indexToSkip) {
                continue;
            }
            int difference = target - arr[i];
            if(map.containsKey(difference)) {
                //We found our solution
                soln.add(arr[i]);
                soln.add(arr[map.get(i)]);
                return soln;
            } else {
                map.put(arr[i], i);
            }
        }
        return null;
    }
}
