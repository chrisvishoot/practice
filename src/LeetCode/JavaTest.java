package LeetCode;
import java.io.*;
import java.util.*;

class Solution {
    public static void main(String[] args) {
        ArrayList<String> stringsIn = new ArrayList<String>();
        ArrayList<String> stringsOut = new ArrayList<String>();

        // Complete the following test using Java and return your working code.
        // Put your code in only this file.
        // Given a random alpha-numeric string with no special characters,
        // reformat the string without adding or removing any characters so that no alphabet characters are
        // adjacent to any other alphabet characters and no numeric characters are adjacent to any other numeric characters,
        // if possible, and return the modified string. If it is not possible to reformat the string in that way,
        // then group all the alphabet characters at the beginning of the string and all numeric characters at the end of
        // the string. In either case keep all alphabet characters in the same order relative to each other before and after the
        // formatting and also keep all numeric characters in the same order releative to each other before and after the formatting.

        // Example input1: "aabb12cc345"
        // Example output1: "a1a2b3b4c5c"
        // Example input2: "a1aa"
        // Example output2: "aaa1"

        stringsIn.add("aabb12cc345");
        stringsIn.add("a1aa");
        stringsIn.add("");
        stringsIn.add("1bbb123456");
        stringsIn.add("123bb");
        stringsIn.add("chris__invalid");
        stringsIn.add("blank white spaces!!! Should return a blank string!");
        stringsIn.add(null);

        stringsOut.add("a1a2b3b4c5c");
        stringsOut.add("aaa1");
        stringsOut.add("");
        stringsOut.add("bbb1123456");
        stringsOut.add("1b2b3");
        stringsOut.add("");
        stringsOut.add("");
        stringsOut.add("");

        int numCorrect = 0;

        for (int i = 0; i < stringsIn.size(); i++) {
            String result = reformatString(stringsIn.get(i));
            System.out.println(result);
            if (result.equals(stringsOut.get(i))) {
                numCorrect++;
            }
        }

        System.out.println("Correct: " + numCorrect + " out of total: " + stringsIn.size());

        return;
    }

    /**
     * Regular expression to see if the string only contains alpha-numeric inputs
     * Found this on Stack Overflow here:
     * https://stackoverflow.com/questions/11241690/regex-for-checking-if-a-string-is-strictly-alphanumeric
     * @param str
     * @return true or false if the string contains only alpha numeric inputs.
     */
    public static boolean isAlphaNumeric(String str) {
        String pattern= "^[a-zA-Z0-9]*$";
        return str.matches(pattern);
    }

    /**
     * Formats string in a way such that the characters would alternate between letters and numbers if possible.
     * Otherwise, it would return a string such that the letters are grouped and clustered together.
     * @param str
     * @return alternating letters and numbers, or clusters of letters and numbers
     */
    public static String reformatString(String str) {
        StringBuilder sb = new StringBuilder();
        //If the input is null, empty, or has invalid characters like white spaces or other non-alphanumeric inputs
        //I just return a blank string.
        //In an ideal world, I would throw an exception detailing and logging the error with an appropiate status code.
        if(str == null || str.length() == 0 || str.isEmpty() || !isAlphaNumeric(str)) {
            return sb.toString();
        }
        //Use a queue of characters and digits to maintain the same order of the string input
        Queue<Character> charQueue = new LinkedList<>();
        Queue<Character> digitQueue = new LinkedList<>();
        for(char c : str.toCharArray()) {
            if(Character.isDigit(c)) {
                digitQueue.add(c);
            } else {
                charQueue.add(c);
            }
        }
        //So there are a few cases to take into account,
        //1) if they are the same size, or have a difference of one
        boolean sameSize = charQueue.size() == digitQueue.size();
        boolean diffOfOne = (charQueue.size() -1) == digitQueue.size() || charQueue.size() == (digitQueue.size() - 1);
        if(sameSize || diffOfOne) {
            boolean firstCharLetter = false;
            if(Character.isLetter(str.charAt(0))) {
                firstCharLetter = true;
            }
            //Then we can inter-weve between the two.
            //abcd1234 is an example input
            //a1b2c3d4 is the output if the two queues are the same
            while(!charQueue.isEmpty() && !digitQueue.isEmpty()) {
                char character = charQueue.remove();
                char digit = digitQueue.remove();
                if(firstCharLetter) {
                    sb.append(character);
                    sb.append(digit);
                } else {
                    sb.append(digit);
                    sb.append(character);
                }

            }
            while(!charQueue.isEmpty()) {
                sb.append(charQueue.remove());
            }
            while(!digitQueue.isEmpty()) {
                sb.append(digitQueue.remove());
            }
            //Otherwise we will do the cluster
        } else {
            while(!charQueue.isEmpty()) {
                sb.append(charQueue.remove());
            }
            while(!digitQueue.isEmpty()) {
                sb.append(digitQueue.remove());
            }
        }
        return sb.toString();
    }
}
