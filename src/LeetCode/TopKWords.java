package LeetCode;

import java.util.*;

public class TopKWords {


    public List<String> getWords(String[] input, int k) {
        HashMap<String, Integer> count = new HashMap<>();
        for(String word : input) {
            if(count.containsKey(word)) {
                int c = count.get(word);
                c++;
                count.put(word, c);
            } else {
                count.put(word, 1);
            }
        }
        PriorityQueue<WordCount> pq = new PriorityQueue<>();
        for(String word : count.keySet()) {
            WordCount wordCount = new WordCount();
            wordCount.word = word;
            wordCount.count = count.get(word);
            System.out.println(wordCount.toString());
            pq.add(wordCount);
        }
        List<String> results = new ArrayList<>(k);
        int i = 0;
        while(i < k) {

            results.add(pq.poll().word);
            i++;
        }

        return results;
    }
    class WordCount implements Comparable<WordCount> {
        int count;
        String word;
        @Override
        public int compareTo(WordCount other) {
            return other.count - this.count;
        }
        @Override
        public String toString() {
            return "Word : " + word + " Count " + count;
        }
    }
}
