package LeetCode;

import java.util.Stack;

public class DailyTemperatures {
    public int[] dailyTemperatures(int[] input) {

        Stack<Integer> stack = new Stack<Integer>();
        if(input == null || input.length <= 1) {
            return new int[]{0};
        }
        int current = 0;
        int[] result = new int[input.length];
        stack.push(input[current]);
        int next = current+1;
        while(current < input.length) {
            if(input[next] > stack.peek()) {
                result[current] = stack.size();
                stack = new Stack<>();
                current++;
                stack.push(input[current]);
            } else {
                stack.push(input[next]);
            }
            next++;
            if(next >= input.length) {
                result[current] = 0;
                stack = new Stack<>();
                current++;
                stack.push(input[current]);
                next = current + 1;
            }
        }
        return result;
    }
}
