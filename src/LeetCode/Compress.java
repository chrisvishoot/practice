package LeetCode;

import java.util.HashMap;
import java.util.Map;

public class Compress {
    public Compress() {

    }

    public int compress(char[] chars) {
        Map<Character, Integer> map = new HashMap<>();
        for(char c : chars) {
            if(map.containsKey(c)) {
                int count = map.get(c);
                count++;
                map.put(c, count);
            } else {
                map.put(c, 1);
            }
        }
        int[] result = new int[map.size()];
        int k = 0;
        return map.size();
    }
}
