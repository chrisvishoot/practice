package LeetCode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class SearchSuggestion {
    public List<List<String>> suggestedProducts(String[] products, String searchWord) {
        Trie trie = new Trie();
        for(String product : products) {
            trie.insert(product);
        }
        List<String> words = trie.getWordList();
        System.out.println(words.toString());
        return null;
    }


    class Trie {
        TrieNode root;
        public Trie() {
            this.root = new TrieNode();
        }
        public void insert(String input) {
            TrieNode current = root;
            for(char c : input.toCharArray()) {
                if(!current.children.containsKey(c)) {
                    current.children.put(c, new TrieNode());
                }
                current = current.children.get(c);
            }
            current.isWord = true;
        }
        public List<String> getWordList() {
            TrieNode current = root;
            List<String> listOfWords = new ArrayList<>();
            for(Character character : current.children.keySet()) {
                TrieNode node = current.children.get(character);
                if(node != null) {
                    getWordList(listOfWords, character + "", node);
                }
            }
            return listOfWords;
        }

        private void getWordList(List<String> listOfWords, String word, TrieNode current) {
            if(current.isWord) {
                listOfWords.add(word);
            }
            for(Character character : current.children.keySet()) {
                word += character;
                current = current.children.get(character);
                getWordList(listOfWords, word, current);
            }
        }

        public boolean contains(String in) {
            TrieNode curr = this.root;
            for(Character c : in.toCharArray()) {
                if(!curr.children.containsKey(c)) {
                    return false;
                }
                curr = curr.children.get(c);
            }
            return true;
        }


    }

    class TrieNode {
        HashMap<Character, TrieNode> children;
        boolean isWord;
        public TrieNode() {
            this.children = new HashMap<>();
            this.isWord = false;
        }

    }

}