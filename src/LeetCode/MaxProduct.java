package LeetCode;

public class MaxProduct {

    public int maxProduct(int[] nums) {
        int[] productArray = new int[nums.length];
        int max = nums[0];
        productArray[0] = nums[0];
        int current = nums[0];
        for(int i = 1; i < nums.length; i++) {
            productArray[i] = nums[i] * productArray[i-1];
            current = nums[i];
            if(current > productArray[i]) {
                max = Math.max(current, max);
            } else {
                max = Math.max(productArray[i], max);
            }
        }
        return max;
    }
}
