package LeetCode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WordTrie {
    TrieNode root;


    public WordTrie() {
        this.root = new TrieNode();
    }


    class TrieNode {
        HashMap<Character, TrieNode> children;
        boolean isWord;
        TrieNode() {
            this.children = new HashMap<>();
        }
    }

    public boolean prefix(String str) {
        if(str == null || str.length() == 0) {
            return false;
        }
        char[] array = str.toCharArray();
        TrieNode current = this.root;
        for(char c : array) {
            if(!current.children.containsKey(c)) {
                return false;
            }
            current = current.children.get(c);
        }
        return true;
    }

    public void add(String word) {
        if(word == null || word.length() == 0) return;
        char[] array = word.toCharArray();
        TrieNode current = this.root;
        for(char character : array) {
            if(!current.children.containsKey(character)) {
                current.children.put(character, new TrieNode());
            }
            current = current.children.get(character);
        }
        current.isWord = true;
    }

    public List<String> getWordsThatStartWith(String str) {
        List<String> words = new ArrayList<>();
        for(Character character : str.toCharArray()) {
            TrieNode current = this.root.children.get(character);
            if(current != null) {
                getWords(current, character + "", words);
            }
        }
        return words;
    }

    public List<String> getWords() {
        List<String> listOfWords = new ArrayList<>();
        for(Character character : this.root.children.keySet()) {
            TrieNode node = this.root.children.get(character);
            if(node != null) {
                getWords(node, character + "", listOfWords);
            }
        }
        return listOfWords;
    }

    private void getWords(TrieNode current, String word, List<String> listOfWords) {
        if(current.isWord) {
            listOfWords.add(word);
        }
        for(Character character : current.children.keySet()) {
             word += character;
             current = current.children.get(character);
             if(current != null) {
                 getWords(current, word, listOfWords);
             }

        }
    }
}
