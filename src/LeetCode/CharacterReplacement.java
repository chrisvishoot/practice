package LeetCode;

import java.util.HashSet;
import java.util.Set;

public class CharacterReplacement {
    public static int findLength(String str, int k) {
        int windowStart = 0;
        int nonRepeat = 0;
        int max = Integer.MIN_VALUE;
        char[] array = str.toCharArray();
        Set<Character> set = new HashSet<>();
        set.add(array[0]);
        // TODO: Write your code here
        for(int windowEnd = 1; windowEnd < array.length; windowEnd++) {
            char character = array[windowEnd];
            if(!set.contains(character)) {
                nonRepeat = windowEnd;
                int count = 0;
                windowEnd += k;
                max = Math.max(max, windowEnd - windowStart + 1);
                windowEnd = nonRepeat;
                windowStart++;
                set.add(character);
            }
            max = Math.max(max, windowEnd - windowStart + 1);
        }
        if(max == Integer.MIN_VALUE) return -1;
        return max;
    }
}
