package LeetCode;

import java.util.Stack;

public class ReverseLinkedList {
    class ListNode {
        int data;
        ListNode next;
        public ListNode(int data) {
            this.data = data;
        }
    }
    public ListNode reverseList(ListNode head) {
        Stack<ListNode> stack = new Stack<ListNode>();
        ListNode curr = head;
        while(curr != null) {
            stack.push(curr);
            curr = curr.next;
        }

        ListNode root = stack.pop();
        ListNode newNode = root;
        while(!stack.isEmpty()) {
            newNode.next = stack.pop();
            newNode = newNode.next;
        }


        return newNode;
    }
}
