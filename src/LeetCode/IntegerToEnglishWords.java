package LeetCode;

import java.util.HashMap;
import java.util.Map;

public class IntegerToEnglishWords {

    public String numberToWords(int num) {
        StringBuilder sb = new StringBuilder();
        int length = getNumberOfDigits(num);
        System.out.println("length is " + length);
        while(length > 0) {

        }
        String thirdPosition = getNthPlacePosition(length);
        System.out.println(thirdPosition);





        return sb.toString();
    }



    public String getNthPlacePosition(int length) {
        if(length >= 10) {
            return "Billion";
        }
        if(length <= 9 && length >= 7) {
            return "Million";
        }
        if(length <= 6 && length >= 4) {
            return "Thousand";
        }
        if(length == 3) {
            return "Hundred";
        }
        return "";
    }

    public int getNumberOfDigits(int number) {
        return (int) (Math.log10(number) + 1);
    }
}
