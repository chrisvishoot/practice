package LeetCode;

public class BitFun {
    public int divide(int a, int b) {
        int sign = ((a < 0) ^ (b < 0)) ? -1:1;
        a = Math.abs(a);
        b = Math.abs(b);
        int quotient = 0;
        while(a >= b) {
            a -= b;
            quotient++;
        }
        return sign * quotient;
    }

}
