package LeetCode;

import java.text.NumberFormat;

public class Atoi {
    String min = String.valueOf(Integer.MIN_VALUE);
    String max = String.valueOf(Integer.MAX_VALUE);
    public int myAtoi(String str) {
        str = str.trim();
        if(str == null || str.length() == 0) return 0;
        boolean sign = true;
        if(str.charAt(0) == '-' || str.charAt(0) == '+') {
            if(str.charAt(0) == '-') {
                sign = false;
            }
            str = str.substring(1);
        }
        if(str.length() == 0) return 0;
        //then see if there are leading zeroes
        int zeroCount = 0;
        while(zeroCount < str.length() && str.charAt(zeroCount) == '0') zeroCount++;

        int digit = 0;
        long number = 0;
        int i = zeroCount;
        while(i < str.length()) {
            char c = str.charAt(i);
            boolean isDigit = Character.isDigit(c);
            System.out.println(c);
            if(isDigit) {
                if(c >= '0' || c <= '9') {
                    number *= 10;
                    number += Character.getNumericValue(c);
                    i++;
                    digit++;
                }
            } else {
                break;
            }

        }
        if(digit > 10) {
            if(sign) return Integer.MAX_VALUE;
            return Integer.MIN_VALUE;
        }
        if(!sign && number + Integer.MIN_VALUE > 0) {
            return Integer.MIN_VALUE;
        }
        if(sign && number >= Integer.MAX_VALUE) {
            return Integer.MAX_VALUE;
        }
        return sign ? (int) number : (int)(number * -1);
    }

    public String getNumbers(String str) {
        StringBuilder sb = new StringBuilder();
        for(Character c : str.toCharArray()) {
            if(isDigit(c)) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public boolean isDigit(char c) {
        return Character.isDigit(c);
    }
}
