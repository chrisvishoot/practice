package LeetCode;

import java.util.ArrayList;
import java.util.List;

public class Suggestion {
    WordTrie wordTrie = new WordTrie();

    public Suggestion() {

    }
    public List<List<String>> suggestedProducts(String[] products, String searchWord) {
        for(String prod : products) {
            wordTrie.add(prod);
        }
        List<List<String>> results = new ArrayList<>();
        String prefix = "";
        for(char c : searchWord.toCharArray()) {
            System.out.println(prefix + c);
            List<String> words = wordTrie.getWordsThatStartWith(prefix + c);
            results.add(words);
            System.out.println(words);
        }
        return results;
    }

}
