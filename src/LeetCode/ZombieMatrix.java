package LeetCode;

import java.util.List;
import java.util.PriorityQueue;

public class ZombieMatrix {
    public int minHours(int rows, int columns, int[][] grid) {
        if(grid == null || grid.length == 0 || grid[0].length == 0) {
            return 0;
        }
        int hours = 0;
        PriorityQueue<int[]> pq = new PriorityQueue<>();
        int total = grid[0].length * grid.length;
        int count = 0;
        for(int i = 0; i < grid.length; i++) {
            for(int j = 0; i < grid[0].length; j++) {
                if(grid[i][j] == 1) {
                    pq.offer(new int[] {i, j});
                    //Counter to keep track of all of the points, once the number of points == total we can also
                    //break out of the below loop
                    count++;
                }
            }
        }
        int[][] dirs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        while(!pq.isEmpty()) {
            int size = pq.size();
            //the current size of the pq will represent the number of points left in this instance
            for(int i = 0; i < size; i++) {
                if(count == total) {
                    return hours;
                }
                //Get the initial point
                int[] point = pq.poll();
                //Go up, down, left right, and if the value is 0, update it to a one
                for(int[] dir : dirs) {
                    int ni = dir[0] + point[0];
                    int nj = dir[1] + point[1];
                    if(ni >= 0 && ni < grid.length && nj >= 0 && nj < grid[0].length && grid[ni][nj] == 0) {
                        count++;
                        pq.offer(new int[] {ni, nj});
                        grid[ni][nj] = 1;
                    }
                }
            }
            hours++;
        }
        return hours;
    }
}
