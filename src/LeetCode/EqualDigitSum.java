package LeetCode;

import java.util.HashMap;

public class EqualDigitSum {
    public int equalDigit(int[] nums) {
        if(nums == null || nums.length <= 1) {
            return -1;
        }
        HashMap<Integer, Integer> map = new HashMap<>();
        int res = -1;
        for(int i = 0; i < nums.length; i++) {
            int digitSum = sum(nums[i]);
            if(!map.containsKey(digitSum)) {
                map.put(digitSum, nums[i]);
            } else {
                int num = map.get(digitSum);
                res = Math.max(res, nums[i] + num);
            }
        }
        return res;

    }
    public int sum(int n) {
        int sum = 0;
        while(n != 0) {
            int digit = n % 10;
            sum += digit;
            n = n/10;
        }
        return sum;
    }
}
