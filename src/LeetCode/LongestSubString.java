package LeetCode;

import java.util.HashSet;
import java.util.Set;

public class LongestSubString {
    public int lengthOfLongestSubstring(String s) {
        if(s == null || s.length() == 0) {
            return 0;
        }
        Set<Character> set = new HashSet<>();
        int max = Integer.MIN_VALUE;
        int max_so_far = 0;
        char[] sArray = s.toCharArray();
        for(int i=0; i < sArray.length; i++) {
            if(!set.contains(sArray[i])) {
                set.add(sArray[i]);
                max_so_far++;
                set.add(sArray[i]);
            } else {
                max = Math.max(max, max_so_far);
                set = new HashSet<>();
                max_so_far = 0;
                i = i-1;
            }
        }
        max = Math.max(max, max_so_far);
        return max;
    }
}
