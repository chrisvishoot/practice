package LeetCode;

import java.util.*;

public class Tarjan {
    HashMap<Integer, List<Integer>> graph = new HashMap<>();
    HashMap<Integer, Integer> timeMap = new HashMap<>();
    HashSet<Integer> visited = new HashSet<>();
    ArrayList<List<Integer>> result = new ArrayList<>();

    public List<List<Integer>> criticalConnections(int n, List<List<Integer>> connections) {
        for(List<Integer> connection : connections) {
            graph.putIfAbsent(connection.get(0), new ArrayList<Integer>());
            graph.get(connection.get(0)).add(connection.get(1));
            graph.putIfAbsent(connection.get(1), new ArrayList<Integer>());
            graph.get(connection.get(1)).add(connection.get(0));
        }
        helper(-1, 0, 0, new HashSet<>());

        return result;
    }

    private void helper(int parent, int node, int time, HashSet<Integer> visited) {
        int currTime = time;
        visited.add(node);
        timeMap.put(node, time);
        time++;
        for(int neighbor : graph.get(node)) {
            if(neighbor == parent) {
                continue;
            }
            if(!visited.contains(node)) {
                helper(node, neighbor, time, visited);
            }
            timeMap.put(node, Math.min(timeMap.get(node), timeMap.get(neighbor)));
            if(currTime < timeMap.get(node)) {
                result.add(Arrays.asList(node, neighbor));
            }
        }

    }

}
