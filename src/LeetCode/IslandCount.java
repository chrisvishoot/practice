package LeetCode;

public class IslandCount {
    public int numIslands(char[][] grid) {
        int count = 0;
        int row = grid.length;
        int col = grid[0].length;
        boolean[][] visited = new boolean[row][col];
        for(int i = 0; i < row; i++) {
            for(int j = 0; j < grid[i].length; j++) {
                if(isSafe(i, j, grid) && !visited[i][j]) {
                    count++;
                    dfs(grid, visited, i, j);
                }
            }
        }
        return count;
    }

    private void dfs(char[][] grid, boolean[][] visited, int row, int col) {
        if(!isSafe(row, col, grid)) {
            return;
        }
        if(visited[row][col]) {
            return;
        }
        visited[row][col] = true;
        //Then we go up down left right.

        //Go up
        dfs(grid, visited, row + 1, col);
        //Go left
        dfs(grid, visited, row, col - 1);
        //Go right
        dfs(grid, visited, row, col + 1);
        //Go down
        dfs(grid, visited, row - 1, col - 1);

    }

    private boolean isSafe(int row, int col, char[][] grid) {
        int colLength = grid[0].length;
        int rowLength = grid.length;
//        if(row >= 0 && row < rowLength && col >= 0 && col < colLength && grid[row][col] == '1') {
//            return true;
//        }
        if(row >= 0 && row < rowLength) {
            System.out.println("passed row");
            if(col >= 0 && col < colLength) {
                System.out.println("Passed Column");
                if(grid[row][col] == '1') {
                    System.out.println("Passed the 1 test");
                    return true;
                }
            }

        }
        return false;
    }
}
