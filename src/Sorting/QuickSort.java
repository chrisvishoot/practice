package Sorting;

import java.util.Arrays;

public class QuickSort {
    public QuickSort() {}


    public void sort(int[] A, int start, int end) {
        if(start < end) {
            int pIndex = partition(A, start, end);
            sort(A, start, pIndex-1);
            sort(A, pIndex + 1, end);
        }
        System.out.println(Arrays.toString(A));
    }

    private int partition(int[] A, int start, int end) {
        int pivot = A[end];
        int pIndex = start;
        for(int i = start; i < end; i++) {
            if(A[i] <= pivot) {
                swap(i, pIndex, A);
                pIndex++;
            }
        }
        swap(pIndex, end, A);
        return pIndex;
    }
    public void swap(int firstIndex, int secondIndex, int[] A) {
        int temp = A[firstIndex];
        A[firstIndex] = A[secondIndex];
        A[secondIndex] = temp;

    }

}
