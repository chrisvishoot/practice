package Sorting;

import java.util.Arrays;

public class MergeSort {
    public MergeSort() {}


    public void mergeSort(int[] A, int n) {
        if(n < 2) {
            return;
        }
        int mid = n/2;
        int[] left = new int[mid];
        int[] right = new int[n - mid];
        for(int i = 0; i < mid; i++) left[i] = A[i];
        for(int i = mid;i<n;i++) right[i-mid] = A[i];

        mergeSort(left, left.length);
        mergeSort(right, n - right.length);
        mergeList(left, right, A);  // Merging L and R into A as sorted list.
        System.out.println(Arrays.toString(A));



    }

    public void mergeList(int[] leftArray, int[] rightArray, int[] answer) {
        int i = 0;
        int j = 0;
        int k = 0;
        while(i < leftArray.length && j < rightArray.length) {
            if(leftArray[i] <= rightArray[j]) {
                answer[k] = leftArray[i];
                i++;
            } else {
                answer[k] = rightArray[j];
                j++;
            }
            k++;
        }
        while(i < leftArray.length) {
            answer[k] = leftArray[i];
            i++;
            k++;
        }
        while(j < rightArray.length) {
            answer[k] = rightArray[j];
            j++;
            k++;
        }

    }
}
