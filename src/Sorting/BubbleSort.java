package Sorting;

public class BubbleSort {
    public void sort(int[] list) {
        for(int i = 0; i < list.length; i++) {
            for(int j = i; j < list.length; j++) {
                if(list[i] > list[j]) {
                    swap(list, i, j);
                }
            }
        }
    }
    public void swap(int[] list, int i, int j) {
        int temp = list[i];
        list[i] = list[j];
        list[j] = temp;
    }
}
