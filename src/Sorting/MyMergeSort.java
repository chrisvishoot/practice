package Sorting;

public class MyMergeSort {
    public MyMergeSort() {};

    public void mergeSort(int[] A, int n) {
        if(n < 2) {
            return;
        }
        int mid = n/2;
        int[] left = new int[mid];
        int[] right = new int[n - mid];
        for(int i = 0; i < left.length; i++) left[i] = A[i];
        for(int i = mid; i < right.length; i++) right[i - mid] = A[i];
        mergeSort(left, left.length);
        mergeSort(right, n -right.length);
        mergeList(A, left, right);
    }

    public void mergeList(int[] A, int[] left, int[] right) {
        int i = 0;
        int j = 0;
        int k = 0;
        while(i < left.length && j < right.length) {
            if(left[i] > right[j]) {
                A[k++] = right[j++];
            } else {
                A[k++] = left[i++];
            }
        }
        while(i < left.length) {
            A[k++] = left[i++];
        }
        while(j < right.length) {
            A[k++] = right[j++];
        }
    }
}
