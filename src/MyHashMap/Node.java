package MyHashMap;

public class Node<Key, Value> {
    public Key key;
    public Value value;
    public Node(Key key, Value value) {
        this.key = key;
        this.value = value;
    }
    public Node() {}

    @Override
    public String toString() {
        return "Key : " + key + " Value : " + value;
    }
}
