package MyHashMap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class MyHashMap<Key, Value> {
    private int capacity = 10;
    private int size = 0;
    private ArrayList<LinkedList<Node<Key, Value>>> list = new ArrayList<LinkedList<Node<Key, Value>>>(capacity);


    public MyHashMap() {
        for(int i = 0; i < capacity; i++) {
            LinkedList<Node<Key, Value>> linkedList = new LinkedList<Node<Key, Value>>();
            list.add(linkedList);
        }
    }

    public Value get(Key key) {
        return getLastElement(key);
    }



    private Value getLastElement(Key key) {
        int index = this.getIndex(key);
        LinkedList<Node<Key,Value>> linkedList = this.list.get(index);
        return linkedList.getLast().value;
    }

    public void add(Key key, Value value) {
        int index = getIndex(key);
        LinkedList<Node<Key,Value>> linkedList = this.list.get(index);
        Node<Key, Value> node = new Node(key, value);
        linkedList.add(node);
        this.list.add(index, linkedList);
        this.size++;
        Double currentSize = new Double(size);
        Double currentCapacity = new Double(capacity);
        if(currentSize / currentCapacity >= 0.70) {
            rehash();
        }
    }

    @Override
    public String toString() {
        return this.list.toString();
    }

    public int getSize() {
        return this.size;
    }

    public int getCapacity() {
        return this.capacity;
    }

    private void rehash() {
            System.out.println("Rehashing!");
            ArrayList<LinkedList<Node<Key, Value>>> temp = this.list;
            this.list = new ArrayList<>();
            this.capacity = this.capacity * 2;
            for(int i = 0; i < this.capacity; i++) {
                LinkedList<Node<Key, Value>> linkedList = new LinkedList<Node<Key, Value>>();
                list.add(linkedList);
            }
            for(LinkedList<Node<Key,Value>> linkedList : temp) {
                Iterator<Node<Key,Value>> itr = linkedList.iterator();
                while(itr.hasNext()) {
                    Node<Key,Value> current = itr.next();
                    int indexToAdd = getIndex(current.key);
                    LinkedList<Node<Key,Value>> newLinkedList = this.list.get(indexToAdd);
                    newLinkedList.add(current);
                    this.list.add(indexToAdd, newLinkedList);
                }
            }
    }

    private int getIndex(Key key) {
        return key.hashCode() % capacity;
    }


}
