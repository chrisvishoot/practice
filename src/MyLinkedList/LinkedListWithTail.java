package MyLinkedList;

public class LinkedListWithTail {
    private Node root;
    private Node tail;

    public LinkedListWithTail(){}
    public void add(int data) {
        if(root == null) {
            root = new Node(data);
            tail = root;
        } else {
            tail.next = new Node(data);
            tail = tail.next;
        }
    }

    public void printList() {
        Node temp = root;
        while(temp != null) {
            System.out.println(temp.data);
            temp = temp.next;
        }
    }
}
