//package MyLinkedList;
//
//import java.util.Stack;
//
//public class MyLinkedList {
//    public Node root;
//    public MyLinkedList(String data) {
//        this.root = new Node(data);
//    }
//
//    public void add(String data) {
//        if(data == null && data.isEmpty()) {
//            System.out.println("Invalid Data");
//            return;
//        }
//        Node newNode = new Node(data);
//        Node current = this.root;
//        while(current.next != null) {
//            current = current.next;
//        }
//        current.next = newNode;
//    }
//
//    public Node reverse(Node head) {
//        Stack<Node> stack = new Stack<Node>();
//        Node current = head;
//        while(current != null) {
//            stack.push(current);
//            current = current.next;
//        }
//        Node root = stack.pop();
//
//        while(!stack.isEmpty()) {
//            root = stack.pop();
//            root = root.next;
//        }
//
//        Node temp = root;
//        while(temp != null) {
//            System.out.println(temp.data);
//            temp = temp.next;
//        }
//
//        return root;
//    }
//
//    public void remove(String data) {
//        if(data == null && data.isEmpty()) {
//            return;
//        }
//        if(this.root.data.equalsIgnoreCase(data)) {
//            this.root = this.root.next;
//            return;
//        }
//        Node current = this.root;
//        Node prev = null;
//        while(current.next != null) {
//            prev = current;
//            current = current.next;
//            if(current.data.equalsIgnoreCase(data)) {
//                prev.next = current.next;
//            }
//        }
//    }
//
//    public void printList() {
//        Node current = this.root;
//        while(current != null) {
//            System.out.println(current.data);
//            current = current.next;
//        }
//    }
//}
