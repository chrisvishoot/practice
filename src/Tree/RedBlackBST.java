package Tree;

import javafx.util.Pair;

public class RedBlackBST<Key extends Comparable<Key>, Value> {
    private static final boolean RED   = true;
    private static final boolean BLACK = false;
    private class Node {
        private Key key;           // key
        private Value val;         // associated data
        private Node left, right;  // links to left and right subtrees
        private boolean color;     // color of parent link
        private int height;          // subtree count

        public Node(Key key, Value val, boolean color, int height) {
            this.key = key;
            this.val = val;
            this.color = color;
            this.height = height;
        }
    }

    private Node root;

    public RedBlackBST() {

    }
    public boolean isRed(Node x) {
        if(x == null) return false;
        return x.color == RED;
    }

    public boolean isEmpty() {
        return root == null;
    }
    public int size() {
        return size(this.root);
    }
    private int size(Node node) {
        if(node == null) return 0;
        return node.height;
    }

    public Value get(Key key) {
        if(key == null) throw new IllegalArgumentException("Invalid input");
        return get(root, key).val;
    }

    private Node get(Node current, Key key) {
        int cmp = key.compareTo(current.key);
        //go right
        if(cmp > 0) current.right = get(current.right, key);
        if(cmp < 0) current.left = get(current.left, key);
        else return current;
        return null;
    }
    public boolean containsKey(Key key) {
        return get(key) != null;
    }


    public Node rotateRight(Node h) {
        Node x = h.left;
        h.left = x.right;
        x.color = x.right.color;
        x.right.color = RED;
        x.height = h.height;
        h.height = size(h.left) + size(h.right) + 1;
        return x;
    }

    public Node rotateLeft(Node h) {
        Node x = h.right;
        h.right = x.left;
        x.color = x.left.color;
        x.left.color = RED;
        x.height = h.height;
        h.height = size(h.left) + size(h.right) + 1;
        return x;
    }

}
