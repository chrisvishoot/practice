package Tree;

import java.util.LinkedList;
import java.util.Queue;

public class Tree {
    public Node root;

    public Tree() {}

    public void add(int data) {
        if(this.root == null) {
            this.root = new Node(data);
            return;
        }
        Node current = this.root;
        this.root = add(current, data);

    }

    public void bfs() {
        Queue<Node> q = new LinkedList<>();
        q.add(this.root);
        while(!q.isEmpty()) {
            Node current = q.remove();
            if(current != null) {
                System.out.println(current.data);
                if(current.left != null) {
                    q.add(current.left);
                }
                if(current.right != null) {
                    q.add(current.right);
                }
            }
        }
    }
    public void delete(int data) {
        this.root = delete(this.root, data);
    }

    public void inOrder() {
        inOrder(this.root);
    }

    public void postOrder() {
        postOrder(this.root);
    }

    public void preOrder() {
        preOrder(this.root);
    }

    public void preOrder(Node current) {
        if(current == null) return;
        System.out.println(current.data);
        preOrder(current.left);
        preOrder(current.right);
    }


    public void postOrder(Node current) {
        if(current == null) return;
        postOrder(current.left);
        postOrder(current.right);
        System.out.println(current.data);
    }

    private void inOrder(Node current) {
        if(current == null) return;
        inOrder(current.left);
        System.out.println(current.data);
        inOrder(current.right);
    }


    /* A recursive function to insert a new key in BST */
    Node delete(Node root, int key)
    {
        /* Base Case: If the tree is empty */
        if (root == null)  return root;

        /* Otherwise, recur down the tree */
        if (key < root.data)
            root.left = delete(root.left, key);
        else if (key > root.data)
            root.right = delete(root.right, key);

            // if key is same as root's key, then This is the node
            // to be deleted
        else
        {
            // node with only one child or no child
            if (root.left == null)
                return root.right;
            else if (root.right == null)
                return root.left;

            // node with two children: Get the inorder successor (smallest
            // in the right subtree)
            root.data = minValue(root.right);

            // Delete the inorder successor
            root.right = delete(root.right, root.data);
        }

        return root;
    }

    public int minValue(Node current) {
        int minValue = 0;
        while(current != null) {
            minValue = current.data;
            current = current.left;
        }
        return minValue;
    }
//    public void delete(int data) {
//        this.root = delete(this.root, data);
//    }

//    private Node delete(Node current, int data) {
//        if(current == null) return null;
//        if(data > current.right.data) {
//            current.right = delete(current.right, data);
//        }
//        if(data < current.left.data) {
//            current.left = delete(current.left, data);
//        }
//
//        if(current.left == null && current.right == null) {
//            return null;
//        }
//        if(current.left == null) {
//            current = current.right;
//            return current;
//        } else if(current.right == null) {
//            current = current.left;
//            return current;
//        } else {
//            Node minNode = findMin(current);
//            current.data = minNode.data;
//            current.right = delete(current.right, data);
//
//        }
//        return current;
//    }

    private Node findMin(Node current) {
        while(current.left != null) {
            current = current.left;
        }
        return current;
    }

    private Node add(Node currentNode, int data) {
        if(currentNode == null) {
            return new Node(data);
        }
        if(data > currentNode.data) {
            //go right
            currentNode.right = add(currentNode.right, data);
        } else if(data < currentNode.data){
            //go left
            currentNode.left = add(currentNode.left, data);
        }
        return currentNode;
    }


}
