package Tree;

import java.util.LinkedList;
import java.util.Queue;

public class BST {
    private Node root;

    public BST() {}

    public void add(int data) {
        this.root = add(root, data);
    }

    public void inOrder() {
        this.inOrder(root);
    }

    private int getBalance(Node node) {
        if(node == null) {
            return 0;
        } else {
            return (getHeight(node.left) - getHeight(node.right));
        }
    }

    private int getHeight(Node node) {
        if(node == null) {
            return 0;
        } else {
            return node.height;
        }
    }

    private int calculateHeight(Node current) {
        if(current == null) {
            return 0;
        }
        return Math.max(calculateHeight(current.left), calculateHeight(current.right)) + 1;
    }



    private int balance(Node leftNode, Node rightNode) {
        return getHeight(leftNode) - getHeight(rightNode);
    }


    private Node rightRotate(Node root) {
        Node newRoot = root.left;
        root.left = newRoot.right;
        newRoot.right = root;
        newRoot.height = calculateHeight(newRoot);
        root.height = calculateHeight(root);
        return newRoot;
    }

    private Node leftRotate(Node root) {
        Node newRoot = root.right;
        root.right = newRoot.left;
        newRoot.left = root;
        newRoot.height = calculateHeight(newRoot);
        root.height = calculateHeight(root);
        return newRoot;
    }


    private Node add(Node current, int data) {
        if(current == null) {
            return new Node(data);
        }
        if(data > current.data) {
            current.right = add(current.right, data);
        } else if(data < current.data) {
            current.left = add(current.left, data);
        } else {
            throw new IllegalArgumentException("Duplicate data");
        }

        int balance = balance(current.left, current.right);

        //Left inbalance
        if(balance > 1) {
            if(getHeight(current.left.left) >= getHeight(current.left.right)) {
                //This is the LL case, where we need to do a "right rotation"
                current = rightRotate(current);
            } else {
                //Then this is the LR case, where we need to do the left rotation and the right rotation
                current.left = leftRotate(current);
                current = rightRotate(current);
            }
        }
        if(balance < -1) {
            if(getHeight(current.right.right) <= getHeight(current.right.left)) {
                current = leftRotate(current);
            } else {
                current = rightRotate(current);
                current = leftRotate(current);
            }
        }


        return current;
    }

    public void insert(int data) {
        root = insert(root, data);
    }

    public Node insert(Node root, int data){
        if(root == null){
            return new Node(data);
        }
        if(root.data <= data){
            root.right = insert(root.right,data);
        }
        else{
            root.left = insert(root.left,data);
        }
        int balance = balance(root.left, root.right);
        if(balance > 1){
            if(getHeight(root.left.left) >= getHeight(root.left.right)){
                root = rightRotate(root);
            }else{
                root.left = leftRotate(root.left);
                root = rightRotate(root);
            }
        }else if(balance < -1){
            if(getHeight(root.right.right) >= getHeight(root.right.left)){
                root = leftRotate(root);
            }else{
                root.right = rightRotate(root.right);
                root = leftRotate(root);
            }
        }
        else{
            root.height = calculateHeight(root);
        }
        return root;
    }


    private void preOrder (Node current) {
        if(current == null) {
            return;
        }
        System.out.println(current.toString());
        preOrder(current.left);
        preOrder(current.right);
    }

    private void postOrder(Node current) {
        if(current == null) {
            return;
        }
        postOrder(current.left);
        postOrder(current.right);
        postOrder(current);
    }

    public void bfs() {
        Queue<Node> q = new LinkedList();
        q.add(this.root);
        while(!q.isEmpty()) {
            Node current = q.poll();
            System.out.println(current.toString());
            if(current != null) {
                if(current.left != null) {
                    q.add(current.left);
                }
                if(current.right != null) {
                    q.add(current.right);
                }
            }

        }
    }

    private void inOrder(Node current) {
        if(current == null) {
            return;
        }
        inOrder(current.left);
        System.out.println(current.data);
        inOrder(current.right);
    }
}
