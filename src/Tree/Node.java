package Tree;

public class Node {
    public int data;
    public Node left;
    public Node right;
    public int height;

    public Node(int data) {
        this.data = data;
    }
    public Node() {}

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                ", left=" + left +
                ", right=" + right +
                ", height=" + height +
                '}';
    }
}
