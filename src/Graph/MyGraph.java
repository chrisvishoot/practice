package Graph;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class MyGraph<Data> {
    private Map<Data, LinkedList<Data>> map;
    public MyGraph() {
        this.map = new HashMap<>();
    }

    public void addVertex(Data source) {
        if(!map.containsKey(source)) {
            this.map.put(source, new LinkedList<Data>());
        }
    }

    public void addEdge(Data source, Data destination, boolean isBiDirectional) {
        LinkedList<Data> edges = this.map.get(source);
        if(edges == null) {
            edges = new LinkedList<Data>();
        }
        edges.add(destination);
        map.put(source, edges);
        if(isBiDirectional) {
            addEdge(destination, source, false);

        }
    }

    public void printMap() {
        for(LinkedList<Data> list : map.values()) {
            System.out.println(list.toString());
        }
    }





}
