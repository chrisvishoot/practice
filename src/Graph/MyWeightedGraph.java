package Graph;

import sun.awt.image.ImageWatched;

import java.util.*;

public class MyWeightedGraph {
        int vertices;
//        int[] blah
        ArrayList<LinkedList<Edge>> adj;
        public MyWeightedGraph(int vertices) {
            this.vertices = vertices;
            this.adj = new ArrayList<LinkedList<Edge>>();
            for(int i = 0; i < vertices; i++) {
                LinkedList<Edge> linkedList = new LinkedList<Edge>();
                this.adj.add(i, linkedList);
            }
        }

        public void addEdge(int source, int destination, int weight) {
            Edge edge = new Edge(source, destination, weight);
            LinkedList<Edge> linkedList = this.adj.get(source);
            linkedList.add(edge);
            this.adj.add(source, linkedList);
        }

        public void printList() {
            for(LinkedList<Edge> list : this.adj) {
                System.out.println(list.toString());
            }
        }

        public void topSort(int source) {
            boolean[] visited = new boolean[this.vertices];
            Stack<Integer> stack = new Stack<>();
            topSort(source, visited, stack);
            System.out.println(stack.toString());

        }

        private void topSort(int source, boolean[] visited, Stack<Integer> stack) {
            visited[source] = true;
            LinkedList<Edge> linkedList = this.adj.get(source);
            Iterator<Edge> itr = linkedList.iterator();
            while(itr.hasNext()) {
                Edge current = itr.next();
                if(!visited[current.source]) {
                    topSort(current.source, visited, stack);
                }
            }
            stack.push(source);

        }


        public void dfs(int source) {
            boolean[] visited = new boolean[this.vertices];
            dfsUtil(source, visited);

        }

        private void dfsUtil(int source, boolean[] visited) {
            visited[source] = true;
            LinkedList<Edge> list = this.adj.get(source);
            Iterator<Edge> itr = list.iterator();
            while(itr.hasNext()) {
                int v = itr.next().source;
                if(!visited[v]) {
                    dfsUtil(v, visited);
                }
            }

        }

        public void bfs(int source) {
            boolean[] visited = new boolean[vertices];
            Queue<Integer> q = new LinkedList();
            q.add(source);
            while(!q.isEmpty()) {
                int currentIndex = q.poll();
                System.out.println("Visited " + currentIndex);
                visited[currentIndex] = true;
                //Then get the linkedlist
                LinkedList<Edge> current = this.adj.get(currentIndex);
                Iterator<Edge> itr = current.iterator();
                while(itr.hasNext()) {
                    Edge edge = itr.next();
                    if(!visited[edge.source]) {
                        q.add(edge.source);
                    }
                }


            }

        }
}
