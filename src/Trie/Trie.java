package Trie;

import java.util.ArrayList;
import java.util.List;

public class Trie {
    private TrieNode root;

    public Trie() {
        this.root = new TrieNode();
    }

    public void insert(String input) {
        TrieNode current = root;
        for(char c : input.toCharArray()) {
            if(!current.children.containsKey(c)) {
                current.children.put(c, new TrieNode());
            }
            current = current.children.get(c);
        }
        current.isWord = true;
    }


    public boolean search(String input) {
        TrieNode current = root;
        for(char c : input.toCharArray()) {
            if(!current.children.containsKey(c)) {
                return false;
            }
            current = current.children.get(c);
        }
        return true;
    }


    public List<String> getWordList() {
        TrieNode current = root;
        List<String> listOfWords = new ArrayList<>();
        for(Character character : current.children.keySet()) {
            TrieNode node = current.children.get(character);
            if(node != null) {
                getWordList(listOfWords, character + "", node);
            }
        }
        return listOfWords;
    }

    public void delete(String word) {
        delete(root, word, 0);

    }

    private boolean delete(TrieNode current, String word, int index) {
        if(index == word.length()) {
            if(!current.isWord) {
                return false;
            }
            current.isWord = false;
            return current.children.size() == 0;
        }
        Character character = word.charAt(index);
        TrieNode node = current.children.get(character);
        if(node == null) {
            return false;
        }
        boolean shouldDelete = delete(node, word, index + 1);
        if(shouldDelete) {
            current.children.remove(character);
            return current.children.size() == 0;
        }
        return false;
    }

    private void getWordList(List<String> listOfWords, String word, TrieNode current) {
        if(current.isWord) {
            listOfWords.add(word);
        }
        for(Character character : current.children.keySet()) {
            word += character;
            current = current.children.get(character);
            getWordList(listOfWords, word, current);
        }
    }

}
